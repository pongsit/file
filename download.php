<?php

// http://localhost/answer_checker/core/file/download.php?file_name=%E0%B8%82%E0%B9%89%E0%B8%AD%E0%B8%AA%E0%B8%AD%E0%B8%9A.pdf&type=pdf&disposition=inline

require_once '../system/init.php'; 

if(empty($_SESSION['file_url'])){
	exit();
}

$file_name = rand(1000,9999);
if(!empty($_GET['file_name'])){
	$file_name = $_GET['file_name'];
}

$data = file_get_contents($_SESSION['file_url']);
if(!empty($_GET['type'])){
	header("Content-type: application/".$_GET['type']);
}else{
	header("Content-type: application/octet-stream");
}

if(!empty($_GET['disposition'])){
	header("Content-disposition: ".$_GET['disposition'].";filename=".$file_name);
}else{
	header("Content-disposition: attachment;filename=".$file_name);
}

echo $data;