<?php
namespace pongsit\file;

class file{
	public $time;
		
	public function __construct(){
		$this->time = new \pongsit\time\time();
	}

	function scan_folder($folder_path){
		if(file_exists($folder_path)){
	    	// remove all the files starting with '.'
		    $removed_hidden = array_filter(scandir($folder_path),function($item) {
					return !(substr($item,0,1) == '.');
			});
			// return with reindexed array, staring from 0
			return array_values($removed_hidden);
		}else{
			error_log($folder_path.' does not exist! See more detail in File class. ');
		}
    }
    
    // zip function
	function zip($source, $destination, $password){
		$password_code = '';
		if(!empty($password)){
			if($this->isPassword($password)){
				$password_code = '-P '.$password.' ';
			}
		}
		
		exec('zip -9 -r -j '.$password_code.$destination.' '.$source);
	}
	
	// remove a file or directory
	function remove($input) {
		// if the input is a directory
		if(is_dir($input)){
			// for each file in the directory
		    foreach(scandir($input) as $file) {
		    	// ignore . and ..
		        if ('.' === $file || '..' === $file) continue;
		        // if it is a directory, recursively call itself
		        if (is_dir("$input/$file")) $this->remove("$input/$file");
		        // if it is a file remove it
		        else unlink("$input/$file");
		    }
		    // remove the empty directory
		    rmdir($input);
		// if it is not a directory and if it is a file
	    }else if (is_file($input) === true){
	    	// remove the file
		    unlink($input);
	    }
	}
	
	function delete_all_with_file_name($file_name){
		$mask = $file_name.'.*';
		array_map('unlink', glob($mask));
	}
	
	function unique_file_name(){
		return $this->time->now('unix').'-'.uniqid();
	}
	
	function count_file($folder_path){
		$fi = new FilesystemIterator($folder_path, FilesystemIterator::SKIP_DOTS);
		return iterator_count($fi);
	}
	
	function upload($files,$folder_name){
		$path = '';
		if(!empty($files['file']['name'])){
			$filename = $this->time->now('unix').'-'.rand(1000,9999).'-'.$files['file']['name'];
			$unique_filename = $filename; 
			
			if(!file_exists($folder_name)){
				exec('mkdir '.$folder_name);
				error_log('Create '.$folder_name.' folder.');
			}
			
			$path = $folder_name.'/'.$filename;
			rename($_FILES['file']['tmp_name'], $path);
			chmod($path, 0644);
		}
		return $path;
	}
}